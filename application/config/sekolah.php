<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$files = glob(APPPATH . '/config/sekolah/*.php');

foreach ($files as $file) {
    require_once($file);   
}