<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

unset($mysql);

// --- customize this area --//
$mysql['host'] = 'localhost';
$mysql['port'] = '3306';
$mysql['username'] = 'root';
$mysql['password'] = '';
$mysql['dbname'] = 'winbie_sms';
$mysql['schoolname'] = 'Sekolah Winbie';

$config['lang'][] = 'english';

// --- do not change this ---//
$config['fingerdb'][] = $mysql;
