<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filterantrian_model extends CI_Model {

    private $all_array = array();
    public $_dbdefault;

    function __construct(){
        $this->_dbdefault = array(
            'dbdriver' => 'pdo',
            'dbprefix' => '',
            'pconnect' => TRUE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
    }
    public function get_filter_keyword($keyword, $keyword2, $locate)
    {
        $fdb = $this->config->item('fingerdb');
        for ($x = 0, $y = count($fdb); $x < $y; $x++) {    
            unset($this->all_array);
            unset($config);
            $config = $this->_dbdefault;
            $config['dsn'] = 'mysql:host=' . $fdb[$x]['host'] . ';port=' . $fdb[$x]['port'] . ';dbname=' . $fdb[$x]['dbname'];
            $config['username'] = $fdb[$x]['username'];
            $config['password'] = $fdb[$x]['password'];
            $schoolname = $fdb[$x]['schoolname'];
            $db = $this->load->database($config, true);
            if ($locate == $fdb[$x]['dbname']) {    
                $db->select('*');
                $db->from('outbox');
                $db->like('TextDecoded', $keyword);
                $db->like('DestinationNumber', $keyword2);
                $db->limit(10);
                $db->order_by('SendingDateTime', 'desc');
                $this->all_array = $db->get()->result();
                $db->close();
                return $this->all_array;
            }
        }
    }

    function autocomplete($title, $locate){
        $fdb = $this->config->item('fingerdb');
        for ($x = 0, $y = count($fdb); $x < $y; $x++) {    
            unset($config);
            $config = $this->_dbdefault;
            $config['dsn'] = 'mysql:host=' . $fdb[$x]['host'] . ';port=' . $fdb[$x]['port'] . ';dbname=' . $fdb[$x]['dbname'];
            $config['username'] = $fdb[$x]['username'];
            $config['password'] = $fdb[$x]['password'];
            $schoolname = $fdb[$x]['schoolname'];
            $db = $this->load->database($config, true);
            if ($locate == $fdb[$x]['dbname']) {    
                $db->like('TextDecoded', $title , 'both');
                $db->or_like('DestinationNumber', $title, 'both');
                $db->limit(10);
                return $db->get('outbox')->result();
            }
        }
    }
    function autocomplete2($title){
        unset($config);
        $config = $this->_dbdefault;
        $fdb = $this->config->item('fingerdb');
        $config['dsn'] = 'mysql:host=' . $fdb[1]['host'] . ';port=' . $fdb[1]['port'] . ';dbname=' . $fdb[1]['dbname'];
        $config['username'] = $fdb[1]['username'];
        $config['password'] = $fdb[1]['password'];
        $schoolname = $fdb[1]['schoolname'];
        $db = $this->load->database($config, true);
        $db->like('TextDecoded', $title , 'both');
        $db->or_like('DestinationNumber', $title, 'both');
        $db->limit(10);
        return $db->get('outbox')->result();
    }
}
