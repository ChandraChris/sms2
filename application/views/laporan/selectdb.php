<main role="main" class="container">
    <div class="card">
      <div class="card-header">
        First Step ...
    </div>
    <div class="card-body">
        <h5 class="card-title">Select Your Database</h5>
        <p class="card-text">First You must Select Database You Want To Search The Data....</p>
        <form action="<?= base_url('form_laporan') ?>" method="post">
            <div class="form-group">
                <select class="form-control form-control-md" name = "database">
                    <?php foreach ($database as $db): ?>
                        <option name="database" value="<?php echo $db; ?>"><?php echo $db; ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <div class="container">
                    <center>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-outline-success btn-md btn-block">
                                <span>
                                    <i class="fas fa-database"></i>
                                    Select Database
                                </span>
                            </button>
                        </div>
                    </center>
                </div>
            </div>
        </form>
    </div>
</div>

</main><!-- /.container -->